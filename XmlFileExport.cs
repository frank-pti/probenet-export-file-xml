/*
 * ProbeNet XML File Export
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using ProbeNet.Export.File;
using System.IO;
using YAXLib;
using System.Xml;
using Internationalization;
using Frank.Helpers.Xml;
using System.ComponentModel;
using System.Collections.Generic;
using System;

namespace ProbeNet.Export.File.Xml
{
    [TranslatableCaption(Constants.ExportDomain, "XML")]
    [FileFilter("probenet.xml", "text/xml")]
    [Description("Xml")]
    public class XmlFileExport: FileExport
    {
        public XmlFileExport (SettingsProvider settingsProvider):
            base(settingsProvider)
        {
        }

        #region implemented abstract members of ProbeNet.Export.File.FileExport
        public override void Export (
            I18n i18n, MultipleSourcesDataModelBase model, IList<IList<string>> measurementIdsForExport, string filename)
        {
            model.MeasurementIdsForExport = measurementIdsForExport;
            using (Stream writeStream = new FileStream(filename, FileMode.Create)) {
                YAXSerializer serializer = new YAXSerializer(model.GetType());
                XmlWriterSettings settings = new XmlWriterSettings() {
                    Indent = true
                };
                using (NamespaceStrippingXmlWriter xmlWriter =
                    new NamespaceStrippingXmlWriter(writeStream, settings, "yaxlib", "http://www.sinairv.com/yaxlib/")) {
                    serializer.Serialize(model, xmlWriter);
                }
            }
        }
        #endregion
    }
}

